package io.github.andhikayuana.kalkulatorbtc;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * @author yuana <andhikayuana@gmail.com>
 * @since 5/29/17
 */

public class ListActivity extends AppCompatActivity {

    private ListView lvSiswa;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        initView();

        initData();
    }

    private void initView() {
        lvSiswa = (ListView) findViewById(R.id.lvSiswa);
    }

    private void initData() {
        Siswa jarjit = new Siswa();
        jarjit.setNama("Jarjit Singh");
        jarjit.setKelas("Kelas 6");

        Siswa mail = new Siswa();
        mail.setNama("Ismail bin Mail");
        mail.setKelas("Kelas 5");

        Siswa upin = new Siswa();
        upin.setNama("Upin");
        upin.setKelas("Kelas 4");

        Siswa ipin = new Siswa();
        ipin.setNama("Ipin");
        ipin.setKelas("Kelas 4");

        ArrayList<Siswa> siswaArrayList = new ArrayList<>();
        siswaArrayList.add(jarjit);
        siswaArrayList.add(mail);
        siswaArrayList.add(upin);
        siswaArrayList.add(ipin);


        SiswaAdapter siswaAdapter = new SiswaAdapter(this, siswaArrayList);
        lvSiswa.setAdapter(siswaAdapter);
    }

    public class Siswa {
        private String nama;
        private String Kelas;

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public String getKelas() {
            return Kelas;
        }

        public void setKelas(String kelas) {
            Kelas = kelas;
        }
    }

    public class SiswaAdapter extends ArrayAdapter<Siswa> {

        public SiswaAdapter(@NonNull Context context, ArrayList<Siswa> siswaArrayList) {
            super(context, 0, siswaArrayList);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            final Siswa item = getItem(position);

            if (convertView == null) {
                convertView = LayoutInflater
                        .from(getContext())
                        .inflate(R.layout.item_siswa, parent, false);
            }

            TextView tvNama = (TextView) convertView.findViewById(R.id.tvNama);
            TextView tvKelas = (TextView) convertView.findViewById(R.id.tvKelas);
            LinearLayout llItemSiswa = (LinearLayout) convertView.findViewById(R.id.llItemSiswa);

            tvNama.setText(item.getNama());
            tvKelas.setText(item.getKelas());

            llItemSiswa.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(ListActivity.this, item.getNama(), Toast.LENGTH_SHORT).show();
                }
            });
            llItemSiswa.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    new AlertDialog
                            .Builder(getContext())
                            .setMessage("Hapus Data ?")
                            .setPositiveButton("Iya", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    remove(item);
                                    notifyDataSetChanged();
                                }
                            })
                            .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .create()
                            .show();

                    return true;
                }
            });

            return convertView;
        }
    }

}
