package io.github.andhikayuana.kalkulatorbtc;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author yuana <andhikayuana@gmail.com>
 * @since 5/17/17
 */

public class CalcActivity extends AppCompatActivity {

    private EditText etNum1;
    private EditText etNum2;
    private Button btnPlus;
    private Button btnMinus;
    private Button btnCross;
    private Button btnDiv;
    private TextView tvResult;
    private int num1 = 0;
    private int num2 = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_calc);

        initView();

        calculate();
    }

    private void initView() {
        etNum1 = (EditText) findViewById(R.id.etNum1);
        etNum2 = (EditText) findViewById(R.id.etNum2);
        btnPlus = (Button) findViewById(R.id.btnPlus);
        btnMinus = (Button) findViewById(R.id.btnMinus);
        btnCross = (Button) findViewById(R.id.btnCross);
        btnDiv = (Button) findViewById(R.id.btnDiv);
        tvResult = (TextView) findViewById(R.id.tvResult);
    }

    private void calculate() {

        btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isValid()) {
                    num1 = Integer.parseInt(etNum1.getText().toString());
                    num2 = Integer.parseInt(etNum2.getText().toString());

                    int result = num1 + num2;
                    tvResult.setText(String.valueOf(result));
                }
            }
        });

        btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    num1 = Integer.parseInt(etNum1.getText().toString());
                    num2 = Integer.parseInt(etNum2.getText().toString());

                    int result = num1 - num2;
                    tvResult.setText(String.valueOf(result));
                }
            }
        });

        btnCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    num1 = Integer.parseInt(etNum1.getText().toString());
                    num2 = Integer.parseInt(etNum2.getText().toString());

                    int result = num1 * num2;
                    tvResult.setText(String.valueOf(result));
                }
            }
        });

        btnDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()) {
                    num1 = Integer.parseInt(etNum1.getText().toString());
                    num2 = Integer.parseInt(etNum2.getText().toString());

                    int result = num1 / num2;
                    tvResult.setText(result);
                }
            }
        });
    }

    private boolean isValid() {
        boolean isValid = true;

        if (TextUtils.isEmpty(etNum1.getText().toString())) {
            Toast.makeText(this, "Can't blank num 1", Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        if (TextUtils.isEmpty(etNum2.getText().toString())) {
            Toast.makeText(this, "Can't blank num 2", Toast.LENGTH_SHORT).show();
            isValid = false;
        }

        return isValid;
    }
}
